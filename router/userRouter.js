
const userRouter = require('express').Router();
const userController = require('../controllers/userController')
const authController = require('../controllers/authController')
// const passport = require('passport')

const restrict = require('../middlewares/restrict')

userRouter.get('/users', userController.index);
userRouter.get('/details/:id', userController.details);
userRouter.get('/new', restrict, userController.new)
userRouter.post('/create', userController.create)
userRouter.get('/delete/:id', userController.delete)
userRouter.get('/update/:id', userController.updateID)
userRouter.post('/update', userController.update)

userRouter.get('/register', (req, res) => res.render('registration'))
userRouter.post('/register', authController.registerAction)

userRouter.get('/login', (req, res) => res.render('login'))
userRouter.post('/login', authController.loginAction)

userRouter.get('/session', authController.userSessionAction)

module.exports = userRouter;