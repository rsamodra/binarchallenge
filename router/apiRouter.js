const apiRouter = require('express').Router();
const apiUserController = require('../controllers/apiUserController')
const apiRoomController = require('../controllers/apiRoomController')
const apiFightController = require('../controllers/apiFightController')

const restrictJwt = require('../middlewares/restrictJwt')

const { user_detail, match_detail } = require('../models');

apiRouter.put('/restapi/update/:id', (req, res) => {
    user_detail.update({
        user_name: req.body.user_name,
        email: req.body.email,
        password: req.body.password,
        active: req.body.active,
        dob: req.body.dob
    },
        {
            where: { id: req.params.id }
        }).then(function (e) {
            res.status(200).send(`Update user ${e.user_name} complete`);
        })
})

apiRouter.get('/restapi/users', (req, res) => {
    user_detail.findAll().then((user) => res.status(200).json(user));
});

apiRouter.get('/restapi/delete/:id', (req, res) => {
    match_detail.destroy({
        where: { id: req.params.id }
    }).then(function () {
        res.status(200).send(`Delete match complete`);
    })
})

apiRouter.post('/restapi/adduser', (req, res) => {
    const { email, password, user_name } = req.body;
    if (!email) {
        res.status(401)
        res.json({ 'Message': '401 Error, Email can not be empty!' });
        return;
    } else if (!password) {
        res.status(401)
        res.json({ 'Message': '401 Error, Password can not be empty!' });
        return;
    } else if (!user_name) {
        res.status(401)
        res.json({ 'Message': '401 Error, user name can not be empty!' });
        return;
    }
    else {

        user_detail.create({
            user_name: req.body.user_name,
            email: req.body.email,
            password: req.body.password,
            dob: req.body.dob,
            active: req.body.active
        })
            .then(function (data) {
                res.send(`Register berhasil silahkan login, ${data.user_name}`);
                // 
            })
    }
});

//register and login API
apiRouter.post('/restapi/register', apiUserController.registerAction)
apiRouter.post('/restapi/login', apiUserController.loginAction)
apiRouter.get('/restapi/profile', restrictJwt, apiUserController.profileAction)
//room API
apiRouter.post('/restapi/room', restrictJwt, apiRoomController.createRoomAction)
apiRouter.get('/restapi/room/:id', restrictJwt, apiRoomController.roomStatus)
//fight API
apiRouter.post('/restapi/fight/:id', restrictJwt, apiFightController.fightAction)


module.exports = apiRouter;