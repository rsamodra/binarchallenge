const express = require("express");
const matchRouter = express.Router();

const matchController = require('../controllers/matchController')

matchRouter.get('/match', matchController.index);
matchRouter.get('/match/new', matchController.matchRender);
matchRouter.post('/match', matchController.matchCreate);
matchRouter.get('/match/details/:id', matchController.details);
matchRouter.get('/match/update/:id', matchController.updateID);
matchRouter.post('/match/update', matchController.update);
matchRouter.get('/match/delete/:id', matchController.delete);

module.exports = matchRouter;