// const express = require("express");
const venueRouter = require("express").Router();

const venueController = require('../controllers/venueController')

venueRouter.get('/venue', venueController.list)
venueRouter.get('/venue/create', venueController.createRender)
venueRouter.post('/venue', venueController.create)
venueRouter.get('/venue/details/:id', venueController.details)
venueRouter.get('/venue/update/:id', venueController.updateRender)
venueRouter.post('/venue/update', venueController.update)
venueRouter.get('/venue/delete/:id', venueController.deleteMe)

module.exports = venueRouter;