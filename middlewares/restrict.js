module.exports = (req, res, next) => {
    if (req.isAuthenticated()) {
        if (req.user.role != "admin") {
            console.log(req.user.role)
            res.redirect("/login")
            return
        }
        return next()
    }
    res.redirect('/login')
}