'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('match_details', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      playerOne: {
        type: Sequelize.INTEGER,
        references: {
          model: "user_details",
          key: "id",
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      },
      playerTwo: {
        type: Sequelize.INTEGER,
        references: {
          model: "user_details",
          key: "id",
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      },
      playerOneSelect: {
        type: Sequelize.STRING
      },
      playerTwoSelect: {
        type: Sequelize.STRING
      },
      result: {
        type: Sequelize.STRING
      },
      venue: {
        type: Sequelize.INTEGER,
        references: {
          model: "match_venues",
          key: "id",
          onUpdate: 'cascade',
          onDelete: 'cascade'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('match_details');
  }
};