const passport = require('passport')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')
const { user } = require('../models')

passport.use(
    new JwtStrategy({
        jwtFromRequest: ExtractJwt.fromHeader('authorization'),
        secretOrKey: 'rahasiaKu'
    },
        (payLoad, done) => {
            user.findByPk(payLoad.id)
                .then(user => done(null, user))
                .catch(err => done(err, false))
        }
    )
)

passport.serializeUser(
    (user, done) => done(null, user.id)
)

passport.deserializeUser(
    async (id, done) => done(null, await user.findByPk(id))
)

module.exports = passport