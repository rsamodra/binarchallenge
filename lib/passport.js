const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const { user } = require('../models')

async function authenticate(user_name, password, done) {
    try {
        const userKu = await user.authenticate({ user_name, password })
        console.log(userKu)
        return done(null, userKu)
    }
    catch (err) {
        return done(null, false, { message: err.message })
    }
}

passport.use(
    new LocalStrategy({ usernameField: 'user_name', passwordField: 'password' }, authenticate)
)

passport.serializeUser(
    (user, done) => done(null, user.id)
)

passport.deserializeUser(
    async (id, done) => done(null, await user.findByPk(id))
)

module.exports = passport