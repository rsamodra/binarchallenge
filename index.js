const express = require('express');
const app = express();
const PORT = 8000;
// const user = require('./user.json');

const session = require('express-session')
const flash = require('express-flash')
const passport = require('./lib/passport')
const passportJwt = require('./lib/passportJwt')

//import router
const userRouter = require("./router/userRouter");
const matchRouter = require("./router/matchRouter");
const venueRouter = require("./router/venueRouter")
const apiRouter = require('./router/apiRouter.js');

//setting request body parser
app.use(express.urlencoded({ extended: false }));
app.use(express.static('assets'));
app.use(express.json());

app.set('view engine', 'ejs');

//=============
app.use(session({
    secret: 'rahasiaKu',
    resave: false,
    saveUnitialized: false
}))

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(passportJwt.initialize())
//=============

// app.use("/users", userRouter);
// app.use("/match", matchRouter);
// app.use("/venue", venueRouter);
app.use(userRouter);
app.use(matchRouter);
app.use(venueRouter);
app.use(apiRouter);

app.get('/', (req, res) => res.render('home'));
app.get('/game', (req, res) => res.render('game'));

app.listen(PORT, () => console.log(`ready at port ${PORT}`));