const { room } = require('../models');

const createRoomAction = (req, res, next) => {
    const { room_name } = req.body
    room.create({ room_name })
        .then(room => {
            res.json({
                id: room.id,
                room_name: room.room_name,
                playerOne: room.playerOne,
                playerTwo: room.playerTwo
            })
        })
        .catch(err => next(err))
}

const roomStatus = (req, res, next) => {
    room.findOne({
        where: { id: req.params.id }
    })
        .then(roomInfo => {
            if (!roomInfo) {
                res.status(404)
                    .send('data Not Found')
                return;
            }
            res.json({
                id: roomInfo.id,
                room_name: roomInfo.room_name,
                playerOne: roomInfo.playerOne,
                playerTwo: roomInfo.playerTwo,
                playerOneSelect: roomInfo.playerOneSelect,
                playerTwoSelect: roomInfo.playerTwoSelect,
                result: roomInfo.result
            })
        })
        .catch(err => next(err))

}

module.exports = {
    createRoomAction, roomStatus
}