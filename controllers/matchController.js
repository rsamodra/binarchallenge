const { user_detail, match_detail, match_venue } = require('../models');

module.exports = {
    index: (req, res) => {
        match_detail.findAll({
            include: [{ model: user_detail, as: 'playerOneData' }, { model: user_detail, as: 'playerTwoData' }, { model: match_venue, as: 'venueData' }]
        })
            .then((matchs) => {
                res.render('match/matchList', { matchs });
            })
            .catch(err => res.status(404).send('error 404 no Data found'))
    },
    matchRender: (req, res) => {
        user_detail.findAll().then((data) => {
            match_venue.findAll().then((venues) => {
                res.render('match/matchCreate', { data, venues });
            });
        });
    },
    matchCreate: (req, res) => {
        let matchResult;
        if (req.body.playerOneSelect == req.body.playerTwoSelect) {
            matchResult = 'Draw';
        } else if (req.body.playerOneSelect == "Rock") {
            matchResult = req.body.playerTwoSelect == "Scissors" ? "Player One win" : "Player Two win";
        } else if (req.body.playerOneSelect == "Scissors") {
            matchResult = req.body.playerTwoSelect == "Paper" ? "Player One win!" : "Player Two win";
        } else if (req.body.playerOneSelect == "Paper") {
            matchResult = req.body.playerTwoSelect == "Rock" ? "Player One win!" : "Player Two win";
        }
        match_detail.create({
            playerOne: req.body.playerOne,
            playerTwo: req.body.playerTwo,
            playerOneSelect: req.body.playerOneSelect,
            playerTwoSelect: req.body.playerTwoSelect,
            result: matchResult,
            venue: req.body.venue,
        })
            .then(function (data) {
                res.redirect('/match')
            });
    },
    details: (req, res) => {
        match_detail.findOne({
            include: [{ model: user_detail, as: 'playerOneData' }, { model: user_detail, as: 'playerTwoData' }],
            where: { id: req.params.id }
        }).then((data) => {
            //error handler
            if (!data) {
                res.status(404).send('data Not Found');
                return;
            }
            res.render('match/matchDetail', { data });
        });
    },
    updateID: (req, res) => {
        match_detail.findOne({
            include: [{ model: user_detail, as: 'playerOneData' }, { model: user_detail, as: 'playerTwoData' }, { model: match_venue, as: 'venueData' }],
            where: { id: req.params.id }
        })
            .then((match) => {
                //error handler
                if (!match) {
                    res.status(404).send('data Not Found');
                    return;
                }
                user_detail.findAll().then((user) => {
                    match_venue.findAll().then((venues) => {
                        res.render('match/matchUpdate', { match, user, venues });
                    });
                })
            });
    },
    update: (req, res) => {
        match_detail.update({
            playerOne: req.body.playerOne,
            playerTwo: req.body.playerTwo,
            playerOneSelect: req.body.playerOneSelect,
            playerTwoSelect: req.body.playerTwoSelect,
            result: req.body.result,
            venue: req.body.venue
        }, {
            where: { id: req.body.id }
        })
            .then(function (data) {
                res.redirect('/match');
            })
    },
    delete: (req, res) => {
        match_detail.destroy({
            where: { id: req.params.id }
        })
            .then(function () {
                res.redirect('/match')
            })
            .catch(err => {
                res.send(`fail to Delete match, due to ${JSON.stringify(err.message, null, 2)}`)
            })
    }

}