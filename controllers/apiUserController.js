const { user } = require('../models');

const registerAction = (req, res, next) => {
    user.register(req.body)
        .then(user => {
            res.json({
                id: user.id,
                user_name: user.user_name,
                role: user.role
            })
        })
        .catch(err => next(err))
}

const loginAction = (req, res, next) => {
    user.authenticate(req.body)
        .then(user => {
            res.json({
                id: user.id,
                user_name: user.user_name,
                token: user.generateToken(),
                role: user.role
            })
        })
        .catch(err => next(err))
}

const profileAction = (req, res, next) => {
    res.json(req.user)
}

module.exports = {
    registerAction, loginAction, profileAction
}