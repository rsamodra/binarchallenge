const { user_detail, user } = require('../models');
const moment = require('moment');

module.exports = {
    index: (req, res) => {
        user_detail.findAll()
            .then((data) => {
                res.render('user/userList', { data })
            })
    },

    details: (req, res) => {
        user_detail.findOne({
            where: { id: req.params.id }
        })
            .then(detail => {
                res.render('user/userDetail', { detail })
            })
            .catch(err => res.status(404).send('error 404 no Data found'))
    },

    new: (req, res) => {
        res.render('user/userCreate')
    },

    create: (req, res) => {
        const { user_name, email, password, dob, active } = req.body;
        user_detail.create({ user_name, email, password, dob, active })
            .then(data => {
                res.redirect(`/details/${data.id}`)
            })
            .catch(err => {
                res.send(`fail to create user, due to ${JSON.stringify(err.message, null, 2)}`)
            })
    },

    delete: (req, res) => {
        user_detail.destroy({
            where: { id: req.params.id }
        })
            .then(data => {
                res.redirect('/users')
            })
            .catch(err => {
                res.send(`fail to Delete user, due to ${JSON.stringify(err.message, null, 2)}`)
            })
    },

    updateID: (req, res) => {
        user_detail.findOne({
            where: { id: req.params.id }
        })
            .then(data => {
                res.render('user/userUpdate', { data, moment })
            })
            .catch(err => res.status(404).send('error 404 no Data found'))

    },

    update: (req, res) => {
        const { user_name, email, password, dob, active } = req.body;
        user_detail.update({ user_name, email, password, dob, active }, {
            where: { id: req.body.id }
        })
            .then(data => {
                res.redirect('/users')
            })
            .catch(err => {
                res.send(`fail to Update user, due to ${JSON.stringify(err.message, null, 2)}`)
            })
    },

}