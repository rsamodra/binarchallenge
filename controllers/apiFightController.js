const { room } = require('../models');

const fightAction = (req, res, next) => {
    // const { room_name } = req.body

    const playerSelect = req.body.hand
    // console.log(room.playerOne)

    room.findOne({
        where: { id: req.params.id }
    })
        .then(roomInfo => {
            if (!roomInfo.playerOne) {
                room.update(
                    {
                        playerOne: req.user.id,
                        playerOneSelect: playerSelect
                    },
                    {
                        where: { id: req.params.id }
                    })
                    .then(() => {
                        // console.log(roomInfo)
                        res.json({
                            id: roomInfo.id,
                            room_name: roomInfo.room_name,
                            playerOne: roomInfo.playerOne,
                            playerOneSelect: roomInfo.playerOneSelect,
                            playerTwo: roomInfo.playerTwo,
                            playerTwoSelect: roomInfo.playerTwoSelect,
                            result: roomInfo.result
                        })
                    })
                    .catch(err => next(err))
            }
            else if (!roomInfo.playerTwo) {
                var result
                if (roomInfo.playerOneSelect == playerSelect) {
                    result = "Draw";
                } else if (roomInfo.playerOneSelect == "Rock") {
                    result = playerSelect == "Scissors" ? "Player One win" : "Player Two win";
                } else if (roomInfo.playerOneSelect == "Scissors") {
                    result = playerSelect == "Paper" ? "Player One win!" : "Player Two win";
                } else if (roomInfo.playerOneSelect == "Paper") {
                    result = playerSelect == "Rock" ? "Player One win!" : "Player Two win";
                }
                console.log(result, roomInfo.playerOneSelect)
                room.update(
                    {
                        playerTwo: req.user.id,
                        playerTwoSelect: playerSelect,
                        result: result
                    },
                    {
                        where: { id: req.params.id }
                    })
                    .then(() => {
                        res.json({
                            id: roomInfo.id,
                            room_name: roomInfo.room_name,
                            playerOne: roomInfo.playerOne,
                            playerOneSelect: roomInfo.playerOneSelect,
                            playerTwo: roomInfo.playerTwo,
                            playerTwoSelect: roomInfo.playerTwoSelect,
                            result: roomInfo.result
                        })
                    })
                    .catch(err => next(err))
            }
            else res.status(400)
                .send('All player has select hand already')
        })
        .catch(err => next(err))

}

module.exports = { fightAction }