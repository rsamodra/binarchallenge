const { user } = require('../models')
const passport = require('../lib/passport')
// const passport = require('passport')

const registerAction = (req, res, next) => {
    user.register(req.body)
        .then(() => res.redirect('/login'))
        .catch(err => next(err))
}

const loginAction = passport.authenticate('local', {
    successRedirect: '/session',
    failureRedirect: '/login',
    failureFlash: true
})

const userSessionAction = (req, res, next) => {
    res.render('session', req.user.dataValues)
}

module.exports = { registerAction, loginAction, userSessionAction }