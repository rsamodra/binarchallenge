const { match_venue } = require('../models');
const list = (req, res) => {
    match_venue.findAll()
        .then(venues => {
            res.render('venue/venueList', { venues });
        })

}
const details = (req, res) => {
    match_venue.findOne({
        where: { id: req.params.id }
    })
        .then(detail => {
            if (!detail) {
                res.status(404).send('data Not Found');
                return;
            }
            res.render('venue/venueDetail', { detail })
        })
        .catch(err => {
            res.send(`no detail found, ${JSON.stringify(err.message, null, 2)}`)
        })
}

const createRender = (req, res) => {
    res.render('venue/venueCreate');
}

const create = (req, res) => {
    const { venue, maximumPlayer } = req.body;
    match_venue.create({ venue, maximumPlayer })
        .then(venueCreate => {
            res.redirect(`/venue/details/${venueCreate.id}`)
        })
        .catch(err => {
            res.send(`fail to create venue, due to ${JSON.stringify(err.message, null, 2)}`)
        })
}

const updateRender = (req, res) => {
    match_venue.findOne({
        where: { id: req.params.id }
    })
        .then(data => {
            if (!data) {
                res.status(404).send('data Not Found');
                return;
            }
            res.render('venue/venueUpdate', { data })
        })
}
const update = (req, res) => {
    const { venue, maximumPlayer } = req.body;

    match_venue.update({ venue, maximumPlayer }, { where: { id: req.body.id } })
        .then(() => {
            res.redirect('/venue')
        })
}
const deleteMe = (req, res) => {
    match_venue.destroy({
        where: { id: req.params.id }
    })
        .then(function () {
            res.redirect('/venue')
        })
}

module.exports = { list, details, createRender, create, deleteMe, updateRender, update }
