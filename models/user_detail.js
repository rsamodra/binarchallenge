'use strict';
const {
  Model
} = require('sequelize');

//add bcrypt
const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  class user_detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    //====start update here
    static #encrypt = (password) => bcrypt.hashSync(password, 10)

    static register = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password)
      return this.create({
        username,
        password: encryptedPassword
      })
    }
    //====end here

  };
  user_detail.init({
    user_id: DataTypes.INTEGER,
    user_name: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    dob: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'user_detail',
  });
  return user_detail;
};