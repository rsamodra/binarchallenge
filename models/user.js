'use strict';
const { Model } = require('sequelize');
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
    //=================
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({ user_name, password, role }) => {
      (!role) ? role = "user" : role = role
      const encryptedPassword = this.#encrypt(password)
      return this.create({
        user_name,
        password: encryptedPassword,
        role: role
      })
    }

    checkPassword = (password) => bcrypt.compareSync(password, this.password)

    generateToken = () => {
      const payLoad = {
        id: this.id,
        user_name: this.user_name
      }
      const secret = 'rahasiaKu'
      return jwt.sign(payLoad, secret)
    }

    static authenticate = async ({ user_name, password }) => {
      try {
        const userKu = await this.findOne({ where: { user_name } })

        if (!userKu) return Promise.reject('invalid username /password')
        if (!userKu.checkPassword(password)) return Promise.reject('invalid username /password')
        // console.log(userKu)
        return Promise.resolve(userKu)
      }
      catch (err) {
        return Promise.reject(err)
      }
    }
    //=================
  };
  user.init({
    user_name: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};