'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class match_detail extends Model {
    static associate(models) {
      // define association here
      models.match_detail.playerOneData = models.match_detail.belongsTo(
        models.user_detail, {
        as: "playerOneData",
        foreignKey: "playerOne"
      }
      );
      models.match_detail.playerTwoData = models.match_detail.belongsTo(
        models.user_detail, {
        as: "playerTwoData",
        foreignKey: "playerTwo"
      }
      );
      models.match_detail.venueData = models.match_detail.belongsTo(
        models.match_venue, {
        as: "venueData",
        foreignKey: "venue",
      }
      );
    }
  };
  match_detail.init({
    playerOne: DataTypes.INTEGER,
    playerTwo: DataTypes.INTEGER,
    playerOneSelect: DataTypes.STRING,
    playerTwoSelect: DataTypes.STRING,
    result: DataTypes.STRING,
    venue: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'match_detail',
  });
  return match_detail;
};