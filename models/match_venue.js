'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class match_venue extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  match_venue.init({
    venue: DataTypes.STRING,
    maximumPlayer: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'match_venue',
  });
  return match_venue;
};