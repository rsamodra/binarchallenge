//implementing class from challenge chapter 4
class Player {
    constructor(hand) {
        this.hand = hand;
    }

    getHand() {
        return this.hand;
    }
}

class Computer extends Player {
    constructor() {
        super("")
    }

    generateHand() {
        let hasil = Math.floor(Math.random() * Math.floor(3));

        if (hasil == 0) {
            return 'Batu';
        }
        else if (hasil == 1) {
            return 'Kertas';
        } else {
            return 'Gunting';
        }
    }
}

class MatchResult {
    constructor(playerOne, com) {
        this.playerOne = playerOne;
        this.com = com;
    }

    getResult(playerOne, com) {

        if (playerOne == com) {
            return 'Draw !';
        } else if (playerOne == "Batu") {
            return com == "Gunting" ? "Player 1 win!" : "COM win!";
        } else if (playerOne == "Gunting") {
            return com == "Kertas" ? "Player 1 win!" : "COM win!";
        } else if (playerOne == "Kertas") {
            return com == "Batu" ? "Player 1 win!" : "COM win!";
        } else {
            return "VS";
        }
    }
}

let playerData = document.getElementsByClassName("player");
let comData = document.getElementsByClassName("com")
let refresh = document.getElementById("refreshBtn");
let gameResult = document.getElementById("gameResult");

refresh.addEventListener("click", function () {
    console.log('game Refreshed')
    refreshBtn();
});

for (let index = 0; index < playerData.length; index++) {
    const select = playerData[index];

    select.addEventListener("click", function (e) {
        refreshBtn();
        resetSelector(playerData);

        select.classList.add("selected");

        let playerOne = new Player(select.id);
        let playerOneSelect = playerOne.getHand();
        console.log("Player One select = " + playerOneSelect);

        let com = new Computer();
        let comSelect = com.generateHand();
        console.log("Computer Generate = " + comSelect);

        let comSelectDom = document.getElementById("com" + comSelect);
        comSelectDom.classList.add("selected");

        let result = new MatchResult();
        result.getResult(playerOneSelect, comSelect);
        let resultData = result.getResult(playerOneSelect, comSelect)
        console.log(updateResult(resultData));
    })
}

function updateResult(data) {
    gameResult.classList.remove("versus");

    if (data == "Player 1 win!") {
        gameResult.innerText = "Player 1 win!";
        gameResult.classList.add("player-win");
    } else if (data == "COM win!") {
        gameResult.innerText = "COM win!";
        gameResult.classList.add("COM-win");
    } else if (data == "Draw !") {
        gameResult.innerText = "Draw !";
        gameResult.classList.add("Draw");
    } else {
        gameResult.innerText = "VS";
        gameResult.classList.add("versus");
    }
    return data;
}
function resetResult() {
    gameResult.innerText = "VS";
    gameResult.className = "versus";
}

function resetSelector(data) {
    for (let i = 0; i < data.length; i++) {
        const select = data[i];
        select.classList.remove("selected");
    }
}

function refreshBtn() {
    resetSelector(playerData);
    resetSelector(comData);
    resetResult();
}
